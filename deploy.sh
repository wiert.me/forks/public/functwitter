#!/usr/bin/env bash

FUNC_NAME="funcTwitter"
FUNC_REGION="europe-west1"
FUNC_MEMORY="128MB"

gcloud functions deploy "$FUNC_NAME" \
    --memory        "$FUNC_MEMORY"   \
    --region        "$FUNC_REGION"   \
    --runtime       nodejs8          \
    --env-vars-file config.yml       \
    --trigger-http
