const fs = require('fs');
const { funcTwitter } = require('./index');

// Crude method to read config.yml into process.env
fs.readFileSync('config.yml')
    .toString()
    .split(/[\r\n]+/)
    .forEach(line => {
        const m = line.match(/^\s*(.+?)\s*:\s*(.+?)\s*$/);
        if (m) process.env[m[1]] = m[2];
    });

// Fake req object
const req = {
    query: {
        secret: process.env.SECRET,
        ...(() => {
            const arg = process.argv[ process.argv.length - 1 ];
            const m = arg.match(/^search=(.+)$/i);
            return m ? { search: m[1] } : { account: arg };
        })()
    }
};

// Fake res object
const res = {
    contentType: (...args) => { console.log('Content-Type: ', ...args); return res; },
    status:      (...args) => { console.log('Status: ', ...args);       return res; },
    send:        console.log,
};

// Call function
funcTwitter(req, res);