# funcTwitter

There are services like https://www.twitrss.me/ which gate Twitter to RSS,
but they sometimes get ratelimited, as you're sharing an API with many other
people.

The alternative is to sign up for your own Twitter developer account and run
your own software to gate between Twitter and RSS, using your own developer
API credentials.

This repository contains software to do that, specifically designed to run
as a Google Cloud Function, so you don't need to run the software on your
own hardware. Google Cloud Functions has a generous permanent free tier, so
it wont even cost you anything.

# Setup

1. You need to have a Google Cloud account, and you need to have the gcloud
   command line tool installed locally, configured and authenticated. You
   can figure out how to do that part yourself.

2. You need to have a Twitter developer account, and to have obtained API
   credentials for the next part. You can also figure this bit out yourself.

3. You then need to write the following (with suitable changes) to config.yml
   at the root of the repo:

```yaml
---

TWITTER_consumer_key:        Your twitter account API consumer_key
TWITTER_consumer_secret:     Your twitter account API consumer_secret
TWITTER_access_token_key:    Your twitter account API access_token_key
TWITTER_access_token_secret: Your twitter account API access_token_secret

SECRET: A secret known only to you for protecting the cloud function
```

4. You need to have NodeJS installed. Once you have, run "npm i" at the root
   of the repo in order to install the required dependencies.

# Testing

Locally fetch and print RSS feed for "mickeyc" account:

```shell
$ npm start -- mickeyc
```

Locally fetch and print RSS feed for the search term "grepular.com OR emailprivacytester.com OR parsemail.org"

```shell
$ npm start -- search="grepular.com OR emailprivacytester.com OR parsemail.org"
```

# Deploy function to google cloud:

```shell
$ npm run deploy
```

The deployment will print out a "httpsTrigger" URL. You can call your function to
fetch the "mickeyc" twitter feed by doing:

```shell
$ curl 'https://THE_HTTPS_TRIGGER_URL?secret=THE_SECRET_FROM_CONFIG_YML&account=mickeyc'
```

And to fetch the "grepular.com OR emailprivacytester.com OR parsemail.org" search feed:

```shell
$ curl 'https://THE_HTTPS_TRIGGER_URL?secret=THE_SECRET_FROM_CONFIG_YML&search=grepular.com%20OR%20emailprivacytester.com%20OR%20parsemail.org'
```